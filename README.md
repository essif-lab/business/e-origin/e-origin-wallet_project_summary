# e-Origin Wallet Project Summary

For more information, please contact: 
yanis.kyriakides@eorigin.eu by email

## Introduction

### About e-Origin

The company e-Origin is specialized in the development of SAAS ("Software as a Service") solutions for customs risk analysis, customs declaration management consulting, digitalisation and integration of IT tools in the customs, logistics, industry and e-commerce sectors.

### e-Origin Wallet

e-Origin Wallet projects aims to deliver a collaboration platform providing trustable information (verifiable credentials) about Products (and Companies); a platform where all parties collaborate to gradually enrich and/or certify Product’s information. 
 
Ideal subprojects to collaborate with : 

* subprojects that could enrich or use this platform that manufacturers, certifying authorities and other logistics chain actors will use
* subprojects that would benefit from collaboration to obtain from EU the emergence of trusted anchor organisations offering all the legal roots necessary to be able to rely on verifiable credentials of such European SSI platform
* subprojects willing to establish a standard Product and Company Scheme

## Summary

### Business Problem

Organizations involved in cross-border trade suffers from difficult access to complete, correct and trustable information about Products and their origin. This problem has 3 main impacts:
* High risk of customs penalties
* Complex data collection & treatment effort slowing down field logistic flows and increasing the cost of (customs storage,...)
* Immense fraud on the market (unfair competition), estimated  to several billions €/year in EU

Main impacted actors are customs declarants & authorities (customs, sanitary, ...), but also exporters & importers, customs, manufacturers,...
Root cause of the problem is the 
* Fear to share confidential information 
* Lack of structured information
* Lack of trust in the collected information
 
If a solution would, on one hand, offer the guarantee to each contributing party (manufacturers in particular) that other parties would only be able to see information details they need and are authorised to, and, on the other hand, offer the guarantee of authenticity of information to consulting parties and the legal power of such data ... all this via efficient real-time digital access ... the problem would be fixed !

This is our promise.

### Technical Solution

e-Origin Wallet, composed of

1. The e-Origin app, composed of
* A user friendly web applications allowing small actors (users) to interact with Product information (Publish, Certify, Consult). The application will also offer the necessary account management features (registration, subscription to necessary supporting business roles and services , ...)
* API for integration with 3rd party systems allowing larger actors to integrate e-Origin with their internal systems (ERP,...)


2. The e-Origin platform, composed of
* Client’s Cloud agents: each party will be able to subscribe for dedicated SSI-agent running in the cloud with its own wallet and file storage. The services are provided by e-Origin.
* Schema/ credential definition catalog: all schema and credentials definitions are maintained in e-Origin to provide a coherent set of supporting rules and policies to issue or verify credentials. The schema definition will be an open standard and will be developed with the community !

All these components contribute to offer an online access to (verified) information on products by multiple parties. The nice about it is that the product will continue to be enriched over time.

## Getting started with the e-Origin Wallet API

You can start testing the e-Origin rest API simply in a few clicks. With this API, you can create a company wallet, associate product wallets, request credentials for your company and products, interact with certifiers or verifiers …. 

To get you familiar with the API and the concept, we provide a tutorial covering multiple scenarios. See [Getting started with the e-Origin API](https://gitlab.grnet.gr/essif-lab/business/e-origin/e-origin-wallet/-/blob/master/Docs/API/Getting%20started%20with%20the%20e-Origin%20API.md) documentation of the e-Origin Wallet.

See the Open API specification for the e-Origin API [here](https://api.e-origin.anais.tech/api-docs).


## Integration with the eSSIF-Lab Functional Architecture and Community

Components developed that are interoperable with the eSSIF-Lab framework:
- [e-Origin Wallet](https://gitlab.grnet.gr/essif-lab/business/e-origin/e-origin-wallet)
- [Product Identity - DID resolver](https://gitlab.grnet.gr/essif-lab/business/e-origin/e-origin-wallet/-/tree/master/Docs/Product%20identifier%20DID%20resolver)
- [GLEIF - Trusted anchor](https://gitlab.grnet.gr/essif-lab/business/e-origin/e-origin-wallet/-/tree/master/Docs/Gleif%20Trust%20anchor)

More information about the architecture and interoperability with other eSSIF-Lab component can be found [here](https://gitlab.grnet.gr/essif-lab/business/e-origin/e-origin-wallet/-/blob/master/Docs/API/img/e-Origin_Architecture.pdf).

Our solution perfectly fits with the business transaction layer described in the eSSIF-Lab functional architecture. The existing libraries to complete SSI role layers, SSI protocols and crypto layers are sufficiently advanced today and is not a source of major concern for us today. 
The main challenge will be mainly in the integration of our solution with a blockchain / distributed ledger recognized by EU which complies with the existing standards (eg. ISO) and recommendations. Current initiatives about EBSI / eSSIF should be seriously analyzed as our project is of general interest to combat fraud and needs to be recognized by EU authorities. 
